export default {
	data: [], // 列表数据
	page: 1, // 当前页码
	last_page: 1, // 最大页码
	size: 8, // 每页记录数
	total: 0, // 总记录数
}