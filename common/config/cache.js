/**
 * @constant {string} LOGIN_STATUS
 * @description 用于表示登录状态的 token
 */
export const LOGIN_STATUS = 'LOGIN_STATUS_TOKEN';
/**
 * @constant {string} USER_INFO
 * @description 存储用户信息的键
 */
export const USER_INFO = 'USER_INFO';
/**
 * @constant {string} BACK_URL
 * @description 存储登录后跳转地址的键
 */
export const BACK_URL = 'login_back_url';
/**
 * @constant {string} STATE_R_KEY
 * @description 小程序登录状态码的键
 */
export const STATE_R_KEY = 'roution_authorize_state';
/**
 * @constant {string} CACHE_LATITUDE
 * @description 存储缓存纬度的键
 */
export const CACHE_LONGITUDE = 'LONGITUDE';
/**
 * @constant {string} HISTORY_SEARCH_LIST
 * @description 存储历史搜索记录的键
 */
export const CACHE_LATITUDE = 'LATITUDE';
/**
 * @constant {string} HISTORY_SEARCH_LIST
 * @description 存储历史搜索记录的键
 */
export const HISTORY_SEARCH_LIST = 'HISTORY_SEARCH_LIST';